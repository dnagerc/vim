# Donde se usa ensamblador?
- Creación de sistemas Operativos
- Creación de herramientas para sistemas embebidos (IoT)
- Creación de drivers
- Creación de compiladores
- Examinar y/o modificar programas ya creados (reversing)

# Componentes de un PC
- CPU
        - ALU
        - Unidad de Control
        - Registros
- Entrada/Salida
- Memoria principal

# CPU
- Multiplexor: Los multiplexores, són circuitos combinacionales que devuelven una única salida de datos 
- Puertas logicas
AND, OR 

#CISC:
- Grupo complejo de instrucciones para ordenadores:
multiplica(a, b)

#RISK:
- Grupo reducido de instrucciones para ordenadores:
1 Cargar a
2 Cargar b
3 multiplica
4 Guardar a

# Registros
Son unidades de memoria que nos permiten almacenar información dentro de nuestro procesador

Un registro es una fila de flipflops

# Entrada / Salida
Bus de control
Bus de datos
Bus de direcciones

# Memoria
Una pila de registros es una memoria
Tipos:
SRAM - Static RAM
- 
DRAM - Dynamic RAM
- Transistor y capacitor (Nota: El capacitor se va descargando a medida que pasa el tiempo)
CL - tiempo de respuesta

