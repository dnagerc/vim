" vimplug url
" https://github.com/junegunn/vim-plug

" Default Directory for plugins: ~/.vim/plugged
call plug#begin()

" Multiple Plug commands can be written in a single line using | separators
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'morhetz/gruvbox'
Plug 'mattn/emmet-vim'

call plug#end()



" Basic Config
syntax on
set background=dark
set number relativenumber
colorscheme gruvbox
set expandtab
set shiftwidth=4
