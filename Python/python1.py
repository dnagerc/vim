# Print normal y corriente
print("Welcome to", end = '!')
a = 'TEST'
b = 1
# Print concatenando Strings
print(a+str(b))
# Definición de función
def someFunction():
    # Variable Global
    global a
    # Variable Local
    f = 'I am learning Python'
    # Cambiando el valor de la variable global
    a = "Some New Text"
    # Imprimiendo variable local
    print(f)
# Llamada de la función
someFunction()
# Eliminamos la varianble
del a

# Tupla vacia 
tupl = ();
# Asignación de elementos en la tupla
tupl1 = (50,);

# Paking and unpacking de tuplas
# PACKING - Metemos valor en la tupla
# UNPACKING - extraemos el valor en variables

x = ("Guru99", 20, "Education") # tuple packing
(company, emp, profile) = x
print(company)
print(emp)
print(profile)
# Comparing tuples
a=(5, 3)
b=(5, 4)

if (a>b): print("a is bigger")
else: print("b is bigger")
dictionary = {}
dictionary["some", "title"] = 2


c = {'x': 100, 'y': 200}
d = list(c.items())

print(d)
for first, last in dictionary:
        print(first, last, dictionary[first, last])


