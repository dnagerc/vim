# Introduction 
This is my first view, model, controller App in Python, i don't know how it will be.

## Some concepts
### Model
- The Representation of database.
### View 
- The Representation of view of the Application.
### Controller
- Interacts between model and view for the user.


**In which will consists my app?**
- I will try to make a Graphical Tic Tac Toe with unittests, i don't know how, but i will.

## BrainStorm
        
- Board
        height
        width
- Cell
        empty
        filled
                player1
                player2
- Player
        Player1
        Player2
- Turn
        Turn1
        Turn2
- Time
        Every 30 seconds, change turn, or you lose

## Other concepts:
### What is TCL?
- TCL is the acronym of Tool Command Language (Lenguaje de Herramientas de Comando), is a scripting language created by JOhn Ousterhout. It have an easy syntax to create applications.
#### What is TCL used for?
- TCL is used to develope prototypes, script applications gui interfaces and tests. The combination of Tcl and Tk is known as Tcl/Tk and it is used to create gui interfaces.
### What is tkinter?
- Python library to make gui applications, it have a TCL interpreter.
#### Important TK concepts:
##### widgets
- Python objects instantiated from classes like ttk., ttk.label, ttk.Button
##### widgets hierarchy
- root < frame < (Button, Label)
- each parent passed as first argument of widget constructor. Example:
```
# parent window
root = Tk()
# frame (parent window, options)
frm = ttk.Frame(root, padding=10)
frm.grid()
# label (frame, options)
ttk.Label(frm, text="Hello World!").grid(column=0, row=0)
ttk.Button(frm, text="Quit", command=root.destroy).grid(column=1, row=0)
# display Window in loop
root.mainloop()
```
##### configuration options
```
ttk.Button(frm, text="Quit", command=root.destroy).grid(column=1, row=0)
```
##### geometry management
grid 
```
frm.grid()
ttk.Label(frm, text="Hello World!").grid(column=0, row=0)
```
##### event loop
- Used to update user interface 
```
root.mainloop()
```
##### equivalent previous example in tcl
```
ttk::frame .frm -padding 10
grid .frm
grid [ttk::label .frm.lbl -text "Hello World!"] -column 0 -row 0
grid [ttk::button .frm.btn -text "Quit" -command "destroy ."] -column 1 -row 0
```
### OK... How do I...? What option does?
#### Some tips:
1. Make sure your documentation corresponds to your Python/Tk version.
2. know exact name of class, option or method that you're using.
3. To find out configuration options available on any widget, call its configure() method which returns a dictionary containing variety of information about each object. Use keys(), to get just names of each option.
```
btn = ttk.Button(frm, ...)
print(btn.configure().keys())     
```
4. Find common methods:
```
print(set(btn.configure().keys()) - set(frm.configure().keys()))
```
5. [Tk reference manual](https://www.tcl.tk/man/tcl8.6/TkCmd/contents.html)
6. Another good reference for basic items [Main widgets](https://coderslegacy.com/python/list-of-tkinter-widgets/)