## Some information about unittest in Python:
1. Make your app, example: cuboid_volume.py
2. Create your app (Add function) cuboid_volume
3. Import unittest
4. Create your test with name starting with test*, example: test_cuboid_volume.py
5. Create class which inherit from unittest.TestCase
6. Create the test, example: 

```
import unittest
from cuboid_volume import * 

class TestCuboid(unittest.TestCase):
    def test_cuboid_volume(self):
        self.assertAlmostEqual(cuboid_volume(2), 8)

```