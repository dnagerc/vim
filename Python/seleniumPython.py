from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import sys


buscar = str(sys.argv[1])
driver = webdriver.Firefox()
driver.get("http://www.google.com")

cookiesButton = driver.find_element(By.ID, "L2AGLb")
cookiesButton.click()

inputElement = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input')

inputElement.send_keys(buscar)
inputElement.send_keys(Keys.ENTER)
