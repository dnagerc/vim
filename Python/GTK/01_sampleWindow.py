import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Wnck', '3.0')
gi.require_version('Gst', '1.0')
gi.require_version('AppIndicator3', '0.1')
from gi.repository import Gtk, GObject, Gdk, Wnck, GdkX11, Gst, AppIndicator3

window = Gtk.window()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
