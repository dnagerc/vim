import unittest
from cuboid_volume import * 

class TestCuboid(unittest.TestCase):
    def test_cuboid_volume(self):
        self.assertAlmostEqual(cuboid_volume(2), 8)
