import tkinter as tk
from turtle import width
class Application(tk.Frame):
        def __init__(self, master=None):
            tk.Frame.__init__(self, master)
            self.grid()
            self.createWidgets()

        def createWidgets(self):
            """
            Recibe n cantidad de numeros
            True o false 
            """
            # Buttons
            self.quitButton = tk.Button(self, text="Quit", command=self.quit)
            self.quitButton.grid()
            # Entry
            self.userEntry = tk.Entry(self, width=45)
            self.userEntry.insert(0, 'Adding Some text')
            self.userEntry.grid()

app = Application()
documentacion = app.createWidgets.__doc__
print(documentacion)
app.master.title('Sample application')
app.mainloop()